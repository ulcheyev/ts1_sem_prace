package GameUsers;

import cz.cvut.fel.sit.battleship.GameField.Board;
import cz.cvut.fel.sit.battleship.GameField.Square;
import cz.cvut.fel.sit.battleship.GameField.SquareStatus;
import cz.cvut.fel.sit.battleship.GameUsers.Player;
import cz.cvut.fel.sit.battleship.GameUsers.RepeatedStrikeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

public class PlayerTest {

    Player player;
    Board board;
    Square square;

    @BeforeEach
    public void setUp() {
        player = mock(Player.class);
        board = mock(Board.class);
        square = mock(Square.class);
    }

    @Test
    public void getStatsString_player1With2StrikesAnd3Misses_correctOutput() {
        when(player.getName()).thenReturn("player1");
        when(player.getStrikes()).thenReturn(2);
        when(player.getMisses()).thenReturn(3);
        doAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                return("Player:" + player.getName() + "\nstrikes: " + player.getStrikes() + "\nmisses: " + player.getMisses());
            }
        }).when(player).getStatsString();
        String result = player.getStatsString();

        String expected = "Player:player1\nstrikes: 2\nmisses: 3";

        verify(player, times(1)).getStatsString();
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void fire_newBoardSquare1And1_true() {
        when(square.getX()).thenReturn(1);
        when(square.getY()).thenReturn(1);
        when(square.getStatus()).thenReturn(SquareStatus.Ship);

        doAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (square.getStatus() == SquareStatus.Ship) {
                    return true;
                }
                return false;
            }
        }).when(player).fire(board, square);

        Assertions.assertTrue(player.fire(board, square));
    }

    @Test
    public void fire_newBoardSquare1And1_false() {
        when(square.getX()).thenReturn(1);
        when(square.getY()).thenReturn(1);
        when(square.getStatus()).thenReturn(SquareStatus.Water);

        doAnswer(new Answer<Boolean>() {
            @Override
            public Boolean answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (square.getStatus() == SquareStatus.Water) {
                    return false;
                }
                return true;
            }
        }).when(player).fire(board, square);

        Assertions.assertFalse(player.fire(board, square));
    }

    @Test
    public void isFiredSquare_newBoardXIs1YIs1_RepeatedStrikeException() throws RepeatedStrikeException {
        when(board.getPosition(1, 1)).thenReturn(square);
        when(board.getPosition(1, 1).getStatus()).thenReturn(SquareStatus.Miss);
        doAnswer(invocationOnMock -> {
            if (board.getPosition(1,1).getStatus() == SquareStatus.Miss) {
                throw new RepeatedStrikeException();
            };
            return null;
        }).when(player).isFiredSquare(board, 1, 1);

        Assertions.assertThrows(RepeatedStrikeException.class, () -> {
            player.isFiredSquare(board, 1, 1);
        });
    }
}
