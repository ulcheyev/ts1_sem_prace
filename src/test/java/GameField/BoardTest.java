package GameField;

import cz.cvut.fel.sit.battleship.GameField.Board;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BoardTest {
    Board board;

    @Test
    public void allShipSunk_newStandardBoardWithoutShips_true() {
        board = new Board();
        board.boardConstuction(true);

        Assertions.assertTrue(board.allShipsSunk());
    }
}
