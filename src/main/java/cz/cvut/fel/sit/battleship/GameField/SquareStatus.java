package cz.cvut.fel.sit.battleship.GameField;
public enum SquareStatus {
    Miss,
    Ship,
    BombedShip,
    TooClose,
    Water
}