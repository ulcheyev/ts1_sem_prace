package cz.cvut.fel.sit.battleship.GameField;

public class Square {

    private SquareStatus status;
    private final int x;
    private final int y;

    // Constructor
    public Square(int x, int y, SquareStatus status) {
        this.status = status;
        this.x = x;
        this.y = y;
    }

    public void setStatus(SquareStatus status) {
        this.status = status;
    }

    public SquareStatus getStatus() {
        return status;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char draw(boolean visible) {
        char square = ' ';
        switch (status) {
            case Ship:
                if (visible)
                    square = 'S';
                else
                    square = 'W';
                break;
            case Miss:
                square = 'M';
                break;
            case Water:
                square = 'W';
                break;
            case TooClose:
                square = 'T';
                break;
            case BombedShip:
                square = 'B';
                break;
        }
        return square;
    }
}
