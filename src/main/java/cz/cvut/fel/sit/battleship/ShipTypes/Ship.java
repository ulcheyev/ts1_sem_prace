package cz.cvut.fel.sit.battleship.ShipTypes;

import cz.cvut.fel.sit.battleship.GameField.*;

import java.util.ArrayList;

public abstract class Ship {

    public Board board;
    public int sideSize = board.sideSize;
    public int size;
    private ArrayList<Square> shipPosition = new ArrayList<Square>();
    public Square fstSquare;
    public boolean isAliveState;
    public Orientation shipOrientation;


    public void setSize(int size) {
        this.size = size;
    }

    public void setFstSquare(Square fstSquare) {
        this.fstSquare = fstSquare;
    }
    
    public void setAliveState(boolean aliveState) {
        isAliveState = aliveState;
    }

    public void setOrientation(Orientation orientation) {
        this.shipOrientation = orientation;
    }

    public int getSize() {
        return size;
    }

    public Square getFstSquare() {
        return fstSquare;
    }

    public boolean isAliveState() {
        return isAliveState;
    }

    public Orientation getShipOrientation() {
        return shipOrientation;
    }

    public void setShipOrientation(Orientation shipOrientation) {
        this.shipOrientation = shipOrientation;
    }


    //Ship Placing
    public boolean placeShip(){
        int x = fstSquare.getX();
        int y = fstSquare.getY();
        int squareStartX = fstSquare.getX() - 1;
        int squareStartY = fstSquare.getY() - 1;
        int squareEndX = fstSquare.getX() + size + 1;
        int squareEndY = fstSquare.getY() + 1;

        try{
            //Exeptions check
            isAvailible();
            isOverlap(board);

            //Building a ship
            switch (shipOrientation){
                case horizontal:
                    for (int j = 0; j < 2; j++) {
                        for(int i = 0; i < size + 2; i++){
                            if(((squareStartX + i > 0) && (squareStartX + i < sideSize))&&((squareStartX + i > squareStartX) && (squareStartX + i < squareEndX))){
                                if (squareStartY + j == squareStartY || squareStartY + j == squareEndY ||(squareStartY + j == y && squareStartX + i == squareStartX) || (squareStartY + j == y && squareStartX + i == squareEndX)){
                                    board.getPosition(squareStartX + i, y + j).setStatus(SquareStatus.TooClose);
                                    shipPosition.add(board.getPosition(squareStartX + i, y + j));
                                } else {
                                    board.getPosition(squareStartX + i, y + j).setStatus(SquareStatus.Ship);
                                    shipPosition.add(board.getPosition(squareStartX + i, y + j));
                                }
                            }
                        }
                    } 
 
                case vertical:
                    for (int j = 0; j < 2; j++) {
                        for(int i = 0; i < size + 2; i++){
                            if(((squareStartY + i > 0) && (squareStartY + i < sideSize))&&((squareStartY + i > squareStartY) && (squareStartY + i < squareEndY))){
                                if (squareStartX + j == squareStartX || squareStartX + j == squareEndX ||(squareStartX + j == x && squareStartY + i == squareStartY) || (squareStartX + j == x && squareStartY + i == squareEndY)){
                                    board.getPosition(x + j, squareStartY + i).setStatus(SquareStatus.TooClose);
                                    shipPosition.add(board.getPosition(x + j, squareStartY + i));
                                } else {
                                    board.getPosition(x + j, squareStartY + i).setStatus(SquareStatus.Ship);
                                    shipPosition.add(board.getPosition(x + j, squareStartY + i));
                                }
                            }
                        }
                    }

            }
            return true;
        }
        catch(Exception e){

            System.err.println(e);
            return false;

        }
    }



    public void isOverlap(Board board) throws OverlapException
    {
        if(getShipOrientation()==Orientation.horizontal)
        {
            int startX = fstSquare.getX();
            int startY = fstSquare.getY();
            for(int i=0; i<size; i++)
                if(board.getPosition(startX+i, startY).getStatus() == SquareStatus.Ship || board.getPosition(startX+i, startY).getStatus() == SquareStatus.TooClose){
                    throw new OverlapException();
                }
        }
        else
        {
            int startX = fstSquare.getX();
            int startY = fstSquare.getY();
            for(int i=0; i<size; i++)
                if(board.getPosition( startX,startY+i).getStatus() == SquareStatus.Ship || board.getPosition( startX,startY+i).getStatus() == SquareStatus.TooClose){
                    throw new OverlapException();
                }
        }
    }

    public void isAvailible() throws UnableException
    {
        if(getShipOrientation()==Orientation.horizontal)
        {
            if(fstSquare.getY()<0 || fstSquare.getY() >= sideSize)
                throw new UnableException();
            if(fstSquare.getX()<0 || fstSquare.getX()+size-1 >= sideSize)
                throw new UnableException();
        }
        else
        {
            if(fstSquare.getX()<0 || fstSquare.getX() >= sideSize)
                throw new UnableException();
            if(fstSquare.getY()<0 || fstSquare.getY()+size-1 >= sideSize)
                throw new UnableException();
        }
    }
}
